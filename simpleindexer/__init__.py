from __future__ import print_function
import argparse

from whoosh.fields import Schema, STORED, ID, KEYWORD, TEXT, NUMERIC, DATETIME
from whoosh.index import create_in, open_dir
import html5lib
from xml.etree import ElementTree as ET
import os.path, sys


def ensure_index(path):
    """ path should be the directory to be created / reinited """
    # Create a Schema
    # See whoosh/docs/build/html/quickstart.html#the-index-and-schema-objects
    # stored=True means that it takes space in the index, also supports highlights
    # schema = Schema(path = ID(unique=True), content=TEXT(stored=True), lineno=NUMERIC(stored=True))
    if not os.path.exists(path):
        os.mkdir(path)
        schema = Schema(path = ID(unique=True,stored=True), text=TEXT)
        ix = create_in(path, schema)
    else:
        ix = open_dir(path)
    return ix

def html_file_text (path):
    with open(path) as f:
        t = html5lib.parse(f, treebuilder="etree", namespaceHTMLElements=False)
        # remove script tags
        for parent in t.getiterator():
            for child in parent:
                if child.tag == "script":
                    parent.remove(child)
        return ET.tostring(t, method="text", encoding="utf-8").decode("utf-8")

def index_html_file (writer, path):
    print (path, file=sys.stderr)
    writer.update_document(path=path.decode("utf-8"), text=html_file_text(path))


def main():
    ap = argparse.ArgumentParser("simpleindexer")
    ap.add_argument("--index", default="index/")
    ap.add_argument("input", nargs="+")
    args = ap.parse_args()
    ix = ensure_index(args.index)
    writer = ix.writer()
    for n in args.input:
        index_html_file(writer, n)
    writer.commit()
