#!/usr/bin/env python

from distutils.core import setup
import os

# def find (p, d):
#     ret = []
#     for b, dd, ff in os.walk(os.path.join(p, d)):

#         for f in ff:
#             if not f.startswith("."):
#                 fp = os.path.join(b, f)
#                 ret.append(os.path.relpath(fp, p))
#     ret.sort()
#     return ret

setup(
    name='Simple Indexer',
    version='0.1.0',
    author='Active Archives Contributors',
    author_email='mm@automatist.org',
    packages=['simpleindexer'],
    package_dir={'simpleindexer': 'simpleindexer'},
    #package_data={'activearchives': find("activearchives", "templates/") + find("activearchives", "data/")},
    # package_data={'makeserver': find("makeserver", "data/")},
    scripts=['bin/simpleindexer'],
    url='http://activearchives.org/Simpleindexer/',
    license='LICENSE.txt',
    description='Simpleindexer indexes',
    # long_description=open('README.md').read(),
    install_requires=[
         "whoosh"
    ]
)
